(* -- Records -- *)

val x = {bar = (1+2,true andalso true), foo = 3+4, baz = (false,"hello")}

(* -- Datatype Bindings -- *)

datatype mytype = DuoInts of int * int
                | Str of string
                | What

val a = DuoInts(1,2)  (* val a = DuoInts (1,2) : mytype *)
val b = Str "hi"      (* val b = Str "hi" : mytype *)
val c = Str           (* val c = fn : string -> mytype *)
val d = What          (* val d = What : mytype *)

(* -- Case Expressions -- *)

fun f (x : mytype) = 
  case x of
    DuoInts(y, z) => y + z
    | Str s => String.size s
    | What => 0

f (DuoInts(1,3))
f (Str "hello")
f What

(* -- Useful datatypes -- *)

datatype suit = Club | Diamond | Heart | Spade
datatype rank = Jack | Queen | King | Ace | Num of int

datatype id = StudentNum of int
            | Name of string * (string option) * string

datatype exp = Constant of int
             | Negate of exp
             | Add of exp * exp
             | Multiply of exp * exp

fun eval e = 
  case e of
    Constant i => i
    | Negate e2 => ~(eval e2)
    | Add(e1,e2) => (eval e1) + (eval e2)
    | Multiply(e1,e2) => (eval e1) * (eval e2)

fun eval (Constant i) = i
  | eval (Negate e2) = ~ (eval e2)
  | eval (Add(e1,e2)) = (eval e1) + (eval e2)
  | eval (Multiply(e1,e2)) = (eval e1) * (eval e2)

fun number_of_adds e =
    case e of
      Constant i => 0
      | Negate e2 => number_of_adds e2
      | Add(e1,e2) => 1 + number_of_adds e1 + number_of_adds e2
      | Multiply(e1,e2) => number_of_adds e1 + number_of_adds e2

val example_exp = Add (Constant 19, Negate (Constant 4))
val example_ans = eval example_exp
val example_addcount = number_of_adds (Multiply(example_exp,example_exp))

(* -- More Expression Example -- *)

fun max_constant e =
  case e of
    Constant i => i
    | Negate e2 => max_constant e2
    | Add(e1,e2) => Int.max(max_constant e1, max_constant e2)
    | Multiply(e1,e2) => Int.max(max_constant e1, max_constant e2)

(* -- Type Synonyms -- *)

type card = suit * rank

val c1 : card = (Diamond, Ace)
val c2 : suit * rank = (Heart, Ace)
val c3 = (Spade, Ace)

fun is_Queen_of_Spade(c : card) = 
  #1 c = Spade andalso #2 c = Queen

fun is_Queen_of_Spade2 c =
  case c of
    (Spade, Queen) => true
    | _ => false

(* -- Lists and Options -- *)

datatype my_int_list = Empty
                     | Cons of int * my_int_list

val x = Cons(4, Cons(23, Cons(208, Empty)))

fun append_my_list(xs, ys) = 
  case xs of
    Empty => ys
    | Cons(x, xs') => Cons(x, append_my_list(xs', ys))

fun inc_or_zero intoption =
  case intoption of
    NONE => 0
    | SOME i => i+1

(* Sum: int list -> int *)
fun sum_list xs =
  case xs of
    [] => 0
    | x::xs' => x + sum_list xs'

(* Append: 'a list * 'a list -> 'a list *)
fun append (xs,ys) =
  case xs of
    [] => ys
    | x::xs' => x :: append(xs',ys)

(* -- Polymorphic Datatypes -- *)

datatype 'a option = NONE | SOME of 'a

datatype 'a mylist = Empty | Cons of 'a * 'a mylist

datatype ('a,'b) tree = 
  Node of 'a * ('a,'b) tree * ('a,'b) tree
  | Leaf of 'b

(* Sum of Tree Node: (int,int) tree -> int *)
fun sum_tree tr = 
  case tr of
    Leaf i => i
    | Node (i,l,r) => i + sum_tree l + sum_tree r

(* Sum of Leave Node: ('a,int) tree -> int *)
fun sum_leave tr =
  case tr of
    Leaf i => i
    | Node (i,l,r) => sum_leave l + sum_leave r

(* Number of Leave Node: ('a,'b) tree -> int *)
fun num_leaves tr =
  case tr of
	  Leaf i => 1
    | Node(i,l,r) => num_leaves l + num_leaves r

(* -- Each-Of Pattern Matching -- *)

fun sum_triple1 (triple : int * int * int) =
  case triple of
    (x,y,z) => z + y + x

fun sum_triple2 triple = 
  let 
    val (x,y,z) = triple
  in
    x + y + z
  end

fun sum_triple3 (x, y, z) = x + y + z

(* -- Polymorphic & Equailty Types -- *)

fun append (xs,ys) =
  case xs of
    [] => ys
    | x::xs' => x :: append(xs',ys)

val ok1 = append(["hi","bye"],["programming","languages"])
val ok2 = append([1,2],[4,5]);
(* val not_ok = append([1,2],["programming","languages"]) *)

(* -- Nested Patterns -- *)

exception ListLengthMismatch

(* don't do this *)
fun old_zip3(l1,l2,l3) = 
  if null l1 andalso null l2 andalso null l3
  then []
  else if null l1 orelse null l2 orelse null l3
  then raise ListLengthMismatch
  else (hd l1, hd l2, hd l3) :: old_zip3(tl l1, tl l2, tl l3)

(* don't do this *)
fun shallow_zip3(l1,l2,l3) = 
  case l1 of
    [] => 
    (case l2 of 
      [] => 
      (case l3 of
        [] => []
        | _ => raise ListLengthMismatch)
      | _ => raise ListLengthMismatch)
    | hd1::tl1 => 
    (case l2 of
      [] => raise ListLengthMismatch
      | hd2::tl2 => 
      (case l3 of
        [] => raise ListLengthMismatch
        | hd3::tl3 => (hd1,hd2,hd3)::shallow_zip3(tl1,tl2,tl3)))

fun zip3 list_triple = 
  case list_triple of
    ([],[],[]) => []
    | (hd1::tl1,hd2::tl2,hd3::tl3) => (hd1,hd2,hd3)::zip3(tl1,tl2,tl3)
    | _ => raise ListLengthMismatch

fun unzip3 lst = 
  case lst of
    [] => ([],[],[])
    | (a,b,c)::tl =>
      let
        val (l1,l2,l3) = unzip3 tl
      in
        (a::l1,b::l2,c::l3)
      end

(* -- More Examples -- *)

(* int list -> bool *)
fun nondecreasing xs = 
  case xs of
  [] => true
  | _::[] => true
  | (x::ys) => (x <= hd ys andalso nondecreasing ys)

datatype sgn = P | N | Z

fun mulsgn = (x1,x2) = 
  let
    fun sign x = if x=0 then Z else if x>0 then P else N
  in
    case (sign x1, sign x2) of
      (Z,_) => Z
      | (_,Z) => Z
      | (P,P) => P
      | (N,N) => P
      | _ => N
  end

fun len xs = 
  case xs of
    [] => 0
    | _::xs' => 1 + len xs'

(* -- Function Patterns -- *)

fun append ([],ys) = ys
  | append (x::xs',ys) = x :: append(xs',ys)

(* -- Exception -- *)

fun hd xs =
  case xs of
  [] => raise List.Empyt
  | x::_ => x

exception MyUndesirableCondition
exception MyOtherException of int * int

fun mydiv (x,y) =
  if y=0
  then raise MyUndesirableCondition
  else x div y 

(* int list * exn -> int *)
fun maxlist (xs,ex) =
  case xs of
    [] => raise ex
    | x::[] => x
    | x::xs' => Int.max(x,maxlist(xs',ex))

val w = maxlist ([3,4,5],MyUndesirableCondition)
  handle MyUndesirableCondition => 6

(* -- Tail Recursion -- *)

fun fact1 n = if n=0 then 1 else n * fact1(n-1)

(* More Efficient *)
fun fact2 n =
  let 
    fun aux(n,acc) = 
      if n=0 
      then acc 
      else aux(n-1,acc*n)
  in
    aux(n,1)
  end

fun sum2 xs =
  let 
    fun f(xs,acc) =
      case xs of
        [] => acc
        | i::xs' => f(xs',i+acc)
  in
    f(xs,0)
  end

fun rev2 xs =
  let fun aux(xs,acc) =
    case xs of
      [] => acc
      | x::xs' => aux(xs', x::acc)
  in
    aux(xs,[])
  end
  