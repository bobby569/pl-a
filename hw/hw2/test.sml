(* Homework2 Simple Test *)
(* These are basic test cases. Passing these tests does not guarantee that your code will pass the actual homework grader *)
(* To run the test, add a new line to the top of this file: use "homeworkname.sml"; *)
(* All the tests should evaluate to true. For example, the REPL should say: val test1 = true : bool *)

val test1a_1 = all_except_option("string", ["string"]) = SOME []
val test1a_2 = all_except_option("string", ["abc", "string", "def"]) = SOME ["abc", "def"]
val test1a_3 = all_except_option("string", []) = NONE
val test1a_4 = all_except_option("string", ["abc", "def"]) = NONE

val test1b_1 = get_substitutions1([["foo"],["there"]], "foo") = []
val test1b_2 = get_substitutions1([["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]],"Fred") = ["Fredrick","Freddie","F"]
val test1b_3 = get_substitutions1([["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]],"Jeff") = ["Jeffrey","Geoff","Jeffrey"]

val test1c_1 = get_substitutions2 ([["foo"],["there"]], "foo") = []
val test1c_2 = get_substitutions2([["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]],"Fred") = ["Fredrick","Freddie","F"]
val test1c_3 = get_substitutions2([["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]],"Jeff") = ["Jeffrey","Geoff","Jeffrey"]

val test1d_1 = similar_names([["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]], {first="Fred", middle="W", last="Smith"}) =
	    [{first="Fred", last="Smith", middle="W"}, 
      {first="Fredrick", last="Smith", middle="W"},
	    {first="Freddie", last="Smith", middle="W"}, 
      {first="F", last="Smith", middle="W"}]

val test2a_1 = card_color(Clubs, Num 2) = Black
val test2a_2 = card_color(Spades, Num 7) = Black
val test2a_3 = card_color(Diamonds, Queen) = Red
val test2a_4 = card_color(Hearts, Ace) = Red

val test2b_1 = card_value(Clubs, Num 2) = 2
val test2b_2 = card_value(Spades, Num 7) = 7
val test2b_3 = card_value(Diamonds, Queen) = 10
val test2b_4 = card_value(Hearts, Ace) = 11

val test2c_1 = remove_card([(Hearts, Ace)], (Hearts, Ace), IllegalMove) = []
val test2c_2 = remove_card([(Hearts, Ace),(Clubs, Ace)], (Hearts, Ace), IllegalMove) = [(Clubs, Ace)]
val test2c_3 = (remove_card([(Hearts, Ace)], (Diamonds, Ace), IllegalMove);false) handle IllegalMove => true

val test2d_1 = all_same_color [(Hearts, Ace),(Hearts, Ace)] = true
val test2d_2 = all_same_color [(Hearts, Ace)] = true
val test2d_3 = all_same_color [] = true
val test2d_4 = all_same_color [(Clubs, Ace),(Hearts, Ace)] = false
val test2d_5 = all_same_color [(Hearts, Ace),(Diamonds, Ace)] = true
val test2d_6 = all_same_color [(Clubs, Ace),(Spades, Num 2),(Hearts, Queen)] = false

val test2e_1 = sum_cards [(Clubs, Num 2),(Clubs, Num 2)] = 4
val test2e_2 = sum_cards [(Clubs, Num 5),(Diamonds, Queen)] = 15
val test2e_3 = sum_cards [(Spades, Ace),(Clubs, King)] = 21
val test2e_4 = sum_cards [(Spades, Ace)] = 11
val test2e_5 = sum_cards [] = 0

val test2f_1 = score([(Hearts, Num 2),(Clubs, Num 4)],10) = 4
val test2f_2 = score([(Hearts, Num 2),(Diamonds, Num 4)],10) = 2
val test2f_3 = score([(Hearts, Num 5),(Clubs, Queen)],10) = 15
val test2f_4 = score([(Hearts, Num 5),(Diamonds, Queen)],10) = 7
val test2f_5 = score([(Spades, Num 2),(Clubs, Num 5)],10) = 1
val test2f_6 = score([(Spades, Ace)],10) = 1

val test2g_1 = officiate([(Hearts, Num 2),(Clubs, Num 4)],[Draw],15) = 6
val test2g_2 = officiate([(Clubs,Ace),(Spades,Ace),(Clubs,Ace),(Spades,Ace)],[Draw,Draw,Draw,Draw,Draw],42) = 3
val test2g_3 = (officiate([(Clubs,Jack),(Spades,Num(8))],[Draw,Discard(Hearts,Jack)],42);false) 
                handle IllegalMove => true

val test3a_1 = score_challenge([(Hearts, Num 2),(Clubs, Num 4)],10) = 4
val test3a_2 = score_challenge([(Hearts, Num 2),(Diamonds, Num 4)],10) = 2
val test3a_3 = score_challenge([(Hearts, Num 5),(Clubs, Queen)],10) = 15
val test3a_4 = score_challenge([(Hearts, Num 5),(Diamonds, Queen)],10) = 7
val test3a_5 = score_challenge([(Spades, Num 2),(Clubs, Num 5)],10) = 1
val test3a_6 = score_challenge([(Spades, Ace)],10) = 1
val test3a_7 = score_challenge([(Spades, Ace)],1) = 0
val test3a_8 = score_challenge([(Spades, Ace),(Clubs, Num 5)],10) = 2
val test3a_9 = score_challenge([(Spades, Ace),(Hearts, Ace)],9) = 7
val test3a_10 = score_challenge([(Spades, Ace),(Hearts, Ace)],10) = 6
val test3a_11 = score_challenge([(Spades, Ace),(Clubs, Num 5)],20) = 2
val test3a_12 = score_challenge([(Spades, Ace),(Clubs, Num 5)],100) = 42
