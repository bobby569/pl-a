(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) = s1 = s2

(* put your solutions for problem 1 here *)

(* 1a *)
fun all_except_option(str : string, strlst : string list) : string list option = 
  case strlst of
    [] => NONE
    | s::lst => 
      if same_string(s, str)
      then SOME lst
      else
        case all_except_option(str, lst) of
          NONE => NONE
          | SOME lst => SOME(s::lst)

(* 1b *)
fun get_substitutions1(sublst : string list list, str : string) : string list = 
  case sublst of
    [] => []
    | xs::sub => 
      (case all_except_option(str, xs) of
        NONE => []
        | SOME lst => lst) @ get_substitutions1(sub, str)

(* 1c *)
fun get_substitutions2(sublst : string list list, str : string) : string list = 
  let
    fun helper(sublst : string list list, ans : string list) =
      case sublst of
        [] => ans
        | xs::sub => 
          case all_except_option(str, xs) of
            NONE => helper(sub, ans)
            | SOME lst => helper(sub, ans @ lst)
  in
    helper(sublst, [])
  end

(* 1d *)
fun similar_names(sublst : string list list, {first:string, middle:string, last:string}) = 
  let
    fun helper(lst : string list, ans) = 
      case lst of
        [] => ans
        | s::sub => helper(sub, ans @ [{first=s, middle=middle, last=last}])
  in
    helper(get_substitutions2(sublst, first), [{first=first, middle=middle, last=last}])
  end

(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw

exception IllegalMove

(* put your solutions for problem 2 here *)

(* 2a *)
fun card_color(c : card) : color = 
  case c of
    (Clubs,_) => Black
    | (Spades,_) => Black
    | _ => Red

(* 2b *)
fun card_value(c : card) : int = 
  case c of
    (_,Num v) => v
    | (_,Ace) => 11
    | _ => 10

(* 2c *)
fun remove_card(cs : card list, c : card, e : exn) : card list =
  case cs of
    [] => raise e
    | c'::cs' =>
      if c = c'
      then cs'
      else c'::remove_card(cs',c,e)

(* 2d *)
fun all_same_color(cs : card list) : bool = 
  case cs of
    [] => true
    | c::[] => true
    | c1::c2::cs' => card_color c1 = card_color c2 andalso all_same_color(c2::cs')

(* 2e *)
fun sum_cards(cs : card list) : int = 
  let
    fun helper(cs : card list, acc : int) = 
      case cs of
        [] => acc
        | c::cs' => helper(cs', card_value c + acc)
  in
    helper(cs, 0)
  end

(* 2f *)
fun score(held : card list, goal : int) : int =
  let
    val total = sum_cards held
    val isSameColor = all_same_color held
  in
    (if total > goal then 3 else 1) * (abs (total - goal)) div (if isSameColor then 2 else 1)
  end

(* 2g *)
fun officiate(cs : card list, ml : move list, goal : int) : int = 
  let
    fun helper(cs : card list, ml : move list, held : card list) : int =
      case ml of
        [] => score(held, goal)
        | m::mlst => 
          case m of
            Draw => 
              (case cs of
                [] => score(held, goal)
                | c::clst =>
                  if sum_cards(c::held) >= goal
                  then score(c::held, goal)
                  else helper(clst, mlst, c::held))
            | Discard c => helper(cs, mlst, remove_card(held,c,IllegalMove))
  in
    helper(cs, ml, [])
  end

(* 3a - score_challenge helper *)
fun cntAce(cs : card list) : int =
  case cs of
    [] => 0
    | (_,Ace)::cs' => 1 + cntAce(cs')
    | _::cs' => cntAce(cs')

fun calcScore(total : int, goal : int) : int =
  (if total > goal then 3 else 1) * (abs (total - goal))

(* 3a - score_challenge *)
fun score_challenge(cs : card list, goal : int) : int =
  let
    val isSameColor = all_same_color cs
    val total = sum_cards cs
    val aceCnt = cntAce cs
  in
    if total <= goal
    then calcScore(total, goal) div (if isSameColor then 2 else 1)
    else
      let
        val value = total - Int.min((total-goal) div 10 + 1, aceCnt) * 10
        val value' = value + (if value + 8 <= goal then Int.min(1,aceCnt) else 0) * 10
      in
        calcScore(value', goal) div (if isSameColor then 2 else 1)
      end
  end

(* 3a - officiate_challenge helper *)
fun sum_cards'(cs : card list) : int = 
  let
    fun helper(cs : card list, acc : int) = 
      case cs of
        [] => acc
        | (_,Ace)::cs' => helper(cs', 1 + acc)
        | c::cs' => helper(cs', card_value c + acc)
  in
    helper(cs, 0)
  end

(* 3a - officiate_challenge *)
fun officiate_challenge(cs : card list, ml : move list, goal : int) : int =
  let
    fun helper(cs : card list, ml : move list, held : card list) : int =
      case ml of
        [] => score_challenge(held, goal)
        | m::mlst => 
          case m of
            Draw => 
              (case cs of
                [] => score_challenge(held, goal)
                | c::clst =>
                  if sum_cards'(c::held) >= goal
                  then score_challenge(c::held, goal)
                  else helper(clst, mlst, c::held))
            | Discard c => helper(cs, mlst, remove_card(held,c,IllegalMove))
  in
    helper(cs, ml, [])
  end
