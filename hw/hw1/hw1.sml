(* 1 *)
fun is_older(t1 : int * int * int, t2 : int * int * int) = 
  if (#1 t1) <> (#1 t2)
  then (#1 t1) < (#1 t2)
  else if (#2 t1) <> (#2 t2)
  then (#2 t1) < (#2 t2)
  else (#3 t1) < (#3 t2)

(* 2 *)
fun number_in_month(dates : (int * int * int) list, mon : int) = 
  if null dates
  then 0
  else (if (#2 (hd dates)) = mon then 1 else 0) + number_in_month(tl dates, mon)

(* 3 *)
fun number_in_months(dates : (int * int * int) list, mons : int list) = 
  if null mons
  then 0
  else number_in_month(dates, hd mons) + number_in_months(dates, tl mons)

(* 4 *)
fun dates_in_month(dates : (int * int * int) list, mon : int) = 
  if null dates
  then []
  else if #2 (hd dates) = mon
  then (hd dates) :: dates_in_month(tl dates, mon)
  else dates_in_month(tl dates, mon)
  
(* 5 *)
fun dates_in_months(dates : (int * int * int) list, mons : int list) = 
  if null mons
  then []
  else dates_in_month(dates, hd mons) @ dates_in_months(dates, tl mons)

(* 6 *)
fun get_nth(arr : string list, n : int) = 
  if n <= 1
  then hd arr
  else get_nth(tl arr, n-1)

(* 7 *)
fun date_to_string(date : int * int * int) = 
  let
    val months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
  in
    get_nth(months, #2 date) ^ " " ^ Int.toString(#3 date) ^ ", " ^ Int.toString(#1 date)
  end

(* 8 *)
fun number_before_reaching_sum(sum : int, arr : int list) = 
  if sum <= hd arr
  then 0
  else 1 + number_before_reaching_sum(sum - hd arr, tl arr)

(* 9 *)
fun what_month(day : int) = 
  let
    val lengths = [31,28,31,30,31,30,31,31,30,31,30,31]
  in
    number_before_reaching_sum(day, lengths) + 1
  end

(* 10 *)
fun month_range(s : int, e : int) = 
  if s > e
  then []
  else what_month s :: month_range(s+1, e)

(* 11 *)
fun oldest(dates : (int * int * int) list) =
  if null dates
  then NONE
  else
    let
      fun older(ds : (int * int * int) list) = 
        if null (tl ds)
        then hd ds
        else
          let
            val d_old = older(tl ds)
          in
            if is_older(hd ds, d_old)
            then hd ds
            else d_old
          end
    in
      SOME (older dates)
    end

fun in_arr(num : int, arr : int list) = 
  if null arr
  then false
  else (num = hd arr) orelse in_arr(num, tl arr)

fun remove_dup(arr : int list) = 
  let
    fun helper(arr : int list, num : int) = 
      if num > 12
      then []
      else (if in_arr(num, arr) then [num] else []) @ helper(arr, num+1)
  in
    helper(arr, 1)
  end

(* 12 *)
fun number_in_months_challenge(arr : (int * int * int) list, mons : int list) = 
  number_in_months(arr, remove_dup mons)

(* 12 *)
fun dates_in_months_challenge(arr : (int * int * int) list, mons : int list) = 
  dates_in_months(arr, remove_dup mons)

fun get_nth_int(arr : int list, n : int) = 
  if n <= 1
  then hd arr
  else get_nth_int(tl arr, n-1)

(* 13 *)
fun reasonable_date(date : int * int * int) = 
  if (#1 date) <= 0 orelse (#2 date) <= 0 orelse (#2 date) > 12 orelse (#3 date) <= 0
  then false
  else
    let
      val year = #1 date
      val is_leap = (year mod 400 = 0) orelse (year mod 100 <> 0 andalso year mod 4 = 0)
      val max_date = if is_leap andalso #2 date = 2 then 29 else get_nth_int([31,28,31,30,31,30,31,31,30,31,30,31], #2 date)
    in
      (#3 date) <= max_date
    end

