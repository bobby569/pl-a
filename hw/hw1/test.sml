(* Homework1 Simple Test *)
(* These are basic test cases. Passing these tests does not guarantee that your code will pass the actual homework grader *)
(* To run the test, add a new line to the top of this file: use "homeworkname.sml"; *)
(* All the tests should evaluate to true. For example, the REPL should say: val test1 = true : bool *)


val test11 = is_older ((1,2,3),(2,3,4)) = true
val test12 = is_older ((1,2,3),(1,2,3)) = false
val test13 = is_older ((1,4,3),(2,3,4)) = true
val test14 = is_older ((2,2,2),(1,3,4)) = false

val test2 = number_in_month ([(2012,2,28),(2013,12,1)],2) = 1

val test3 = number_in_months ([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4]) = 3

val test4 = dates_in_month ([(2012,2,28),(2013,12,1)],2) = [(2012,2,28)]

val test5 = dates_in_months ([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4]) = [(2012,2,28),(2011,3,31),(2011,4,28)]

val test61 = get_nth (["hi", "there", "how", "are", "you"], 2) = "there"
val test62 = get_nth (["hi", "there", "how", "are", "you"], 1) = "hi"
val test63 = get_nth (["hi", "there", "how", "are", "you"], 5) = "you"

val test7 = date_to_string (2013, 6, 1) = "June 1, 2013"

val test81 = number_before_reaching_sum (10, [1,2,3,4,5]) = 3
val test82 = number_before_reaching_sum (5, [1,2,3,4,5]) = 2
val test83 = number_before_reaching_sum (2, [1,2,3,4,5]) = 1

val test91 = what_month 70 = 3
val test92 = what_month 1 = 1
val test93 = what_month 31 = 1
val test94 = what_month 32 = 2
val test95 = what_month 59 = 2
val test96 = what_month 60 = 3
val test97 = what_month 365 = 12

val test101 = month_range (31, 34) = [1,2,2,2]
val test102 = month_range (34, 34) = [2]
val test103 = month_range (35, 34) = []

val test111 = oldest([(2012,2,28),(2011,3,31),(2011,4,28)]) = SOME (2011,3,31)
val test112 = oldest([]) = NONE

val test121 = in_arr(3, []) = false
val test122 = in_arr(3, [2,4]) = false
val test123 = in_arr(3, [2,3,4]) = true

val test131 = remove_dup([]) = []
val test132 = remove_dup([1,2,3]) = [1,2,3]
val test133 = remove_dup([1,2,1,3]) = [1,2,3]
val test134 = remove_dup([1,2,3,3,2,1,6,5,4,4,5,6]) = [1,2,3,4,5,6]

val test141 = number_in_months_challenge([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4,4,2]) = 3

val test151 = dates_in_months_challenge([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4,4,2]) = [(2012,2,28),(2011,3,31),(2011,4,28)]

val test161 = reasonable_date((1999, 8, 3)) = true
val test162 = reasonable_date((1999, 8, 0)) = false
val test163 = reasonable_date((1999, 8, 32)) = false
val test164 = reasonable_date((1999, 0, 3)) = false
val test165 = reasonable_date((1999, 13, 3)) = false
val test166 = reasonable_date((0, 8, 3)) = false
val test167 = reasonable_date((0, 8, 3)) = false
val test168 = reasonable_date((1999, 11, 31)) = false
val test169 = reasonable_date((1999, 2, 29)) = false
val test1610 = reasonable_date((2000, 2, 29)) = true
val test1611 = reasonable_date((2004, 2, 29)) = true
