(* Homework3 Simple Test*)
(* These are basic test cases. Passing these tests does not guarantee that your code will pass the actual homework grader *)
(* To run the test,add a new line to the top of this file: use "homeworkname.sml"; *)
(* All the tests should evaluate to true. For example,the REPL should say: val test1 = true : bool *)

(* Credit: Partial test cases are provided by Chris Klopfenstein *)

val test1_1 = only_capitals ["A","B","C"] = ["A","B","C"]
val test1_2 = only_capitals [] = []
val test1_3 = only_capitals ["ABC","Abc","aBC","abc"] = ["ABC","Abc"]
val test1_4 = only_capitals ["a","B","C"] = ["B","C"]
val test1_5 = only_capitals ["1AB","?AB","Abc","ABc","abC"] = ["Abc","ABc"]

val test2_1 = longest_string1 ["A","bc","C"] = "bc"
val test2_2 = longest_string1 [] = ""
val test2_3 = longest_string1 ["ab","cd","efg"] = "efg"
val test2_4 = longest_string1 ["abc","cd","fg"] = "abc"
val test2_5 = longest_string1 ["abc","cd","efg"] = "abc"
val test2_6 = longest_string1 ["A","bc","C","de"] = "bc"
val test2_7 = longest_string1 ["A","bc","C","def"] = "def"

val test3_1 = longest_string2 ["A","bc","C"] = "bc"
val test3_2 = longest_string2 [] = ""
val test3_3 = longest_string2 ["ab","cd","efg"] = "efg"
val test3_4 = longest_string2 ["abc","cd","fg"] = "abc"
val test3_5 = longest_string2 ["abc","cd","efg"] = "efg"
val test3_6 = longest_string2 ["A","bc","C","de"] = "de"
val test3_7 = longest_string2 ["A","bc","C","def"] = "def"

val test4a_1 = longest_string3 ["A","bc","C"] = "bc"
val test4a_2 = longest_string3 ["A","bc","C"] = "bc"
val test4a_3 = longest_string3 [] = ""
val test4a_4 = longest_string3 ["ab","cd","efg"] = "efg"
val test4a_5 = longest_string3 ["abc","cd","fg"] = "abc"
val test4a_6 = longest_string3 ["abc","cd","efg"] = "abc"
val test4a_7 = longest_string3 ["A","bc","C","de"] = "bc"
val test4a_8 = longest_string3 ["A","bc","C","def"] = "def"

val test4b_1 = longest_string4 ["A","B","C"] = "C"
val test4b_2 = longest_string4 ["A","bc","C"] = "bc"
val test4b_3 = longest_string4 [] = ""
val test4b_4 = longest_string4 ["ab","cd","efg"] = "efg"
val test4b_5 = longest_string4 ["abc","cd","fg"] = "abc"
val test4b_6 = longest_string4 ["abc","cd","efg"] = "efg"
val test4b_7 = longest_string4 ["A","bc","C","de"] = "de"
val test4b_8 = longest_string4 ["A","bc","C","def"] = "def"

val test5_1 = longest_capitalized ["A","bc","C"] = "A"
val test5_2 = longest_capitalized ["ABC","Abc","aBC","abc"] = "ABC"
val test5_3 = longest_capitalized ["ab","cd","efg"] = ""
val test5_4 = longest_capitalized [] = ""
val test5_5 = longest_capitalized ["ab","a","b"] = ""

val test6_1 = rev_string "abc" = "cba"
val test6_2 = rev_string "" = ""
val test6_3 = rev_string "ABCcba" = "abcCBA"

val test7_1 = first_answer (fn x => if x > 3 then SOME x else NONE) [1,2,3,4,5] = 4
val test7_2 = (first_answer (fn x => if x > 3 then SOME x else NONE) [];false) handle NoAnswer => true
val test7_3 = (first_answer (fn x => if x > 3 then SOME x else NONE) [0,1,2,3];false) handle NoAnswer => true
val test7_4 = first_answer (fn x => if x > 3 then SOME x else NONE) [8,7,6,5] = 8
val test7_5 = first_answer (fn x => if x > 3 then SOME x else NONE) [4,2,3,5] = 4
val test7_6 = (first_answer (fn x => if x > 3 then SOME x else NONE) [1,2,3];false) handle NoAnswer => true
val test7_7 = (first_answer (fn x => if x > 3 then SOME x else NONE) [1,2,3];false) handle OtherException => true
val test7_8 = first_answer (fn x => if x > 3 then SOME x else NONE) [1,2,3,4,2] = 4

val test8_1 = all_answers (fn x => if x = 1 then SOME [x] else NONE) [2,3,4,5,6,7] = NONE
val test8_2 = all_answers (fn x => if x > 1 then SOME [x] else NONE) [3,2,1] = NONE
val test8_3 = all_answers (fn x => if x > 1 then SOME [x] else NONE) [2,3,4] = SOME [2,3,4]
val test8_4 = all_answers (fn x => if x = 1 then SOME [x] else NONE) [] = SOME []
val test8_5 = all_answers (fn x => if x = 2 then SOME [x] else NONE) [3,2,4,5,6,7] = NONE
val test8_6 = all_answers (fn x => if x mod 2 = 0 then SOME [x] else NONE) [2,4,5,6,8] = NONE
val test8_7 = all_answers (fn x => if x mod 2 = 0 then SOME [x] else NONE) [2,4,6,8] = SOME [2,4,6,8]
val test8_8 = all_answers (fn x => if x mod 2 = 0 then SOME [x,x+1] else NONE) [2,4,6,8] = SOME [2,3,4,5,6,7,8,9]
val test8_9 = all_answers (fn x => if x mod 2 = 0 then SOME [] else NONE) [2,4,6,8] = SOME []
val test8_10 = all_answers (fn x => if x mod 2 = 0 then SOME [x] else NONE) [] = SOME []

val test9a_1 = count_wildcards (Wildcard) = 1
val test9a_2 = count_wildcards (Variable "abc") = 0
val test9a_3 = count_wildcards (UnitP) = 0
val test9a_4 = count_wildcards (ConstP 10) = 0
val test9a_5 = count_wildcards (TupleP [Wildcard,UnitP,Wildcard]) = 2
val test9a_6 = count_wildcards (ConstructorP("a",Wildcard)) = 1
val test9a_7 = count_wildcards (Variable "str") = 0
val test9a_8 = count_wildcards (TupleP [Wildcard,ConstP 12,Wildcard]) = 2
val test9a_9 = count_wildcards (ConstructorP("pattern",(TupleP [Wildcard,ConstP 12,Wildcard]))) = 2

val test9b_1 = count_wild_and_variable_lengths (Variable("a")) = 1
val test9b_2 = count_wild_and_variable_lengths (Wildcard) = 1
val test9b_3 = count_wild_and_variable_lengths (Variable "abc") = 3
val test9b_4 = count_wild_and_variable_lengths (UnitP) = 0
val test9b_5 = count_wild_and_variable_lengths (ConstP 10) = 0
val test9b_6 = count_wild_and_variable_lengths (TupleP [Wildcard,Variable "abc",Wildcard]) = 5
val test9b_7 = count_wild_and_variable_lengths (ConstructorP("abc",Variable "hello")) = 5
val test9b_8 = count_wild_and_variable_lengths Wildcard = 1
val test9b_9 = count_wild_and_variable_lengths (TupleP [Wildcard,ConstP 12,Wildcard]) = 2
val test9b_10 = count_wild_and_variable_lengths (TupleP [Wildcard,Variable "str",Wildcard]) = 5
val test9b_11 = count_wild_and_variable_lengths (TupleP [Wildcard,Variable "str",Wildcard,Variable "str2"]) = 9
val test9b_12 = count_wild_and_variable_lengths (ConstructorP("pattern",(TupleP [Wildcard,ConstP 12,Wildcard]))) = 2
val test9b_13 = count_wild_and_variable_lengths (ConstructorP("pattern",(TupleP [Wildcard,Variable "str",Wildcard]))) = 5

val test9c_1 = count_some_var ("x",Variable("x")) = 1
val test9c_2 = count_some_var ("x",ConstructorP("x",Variable "y")) = 0
val test9c_3 = count_some_var ("x",ConstructorP("y",Variable "x")) = 1
val test9c_4 = count_some_var ("x",UnitP) = 0
val test9c_5 = count_some_var ("x",(TupleP [Wildcard,ConstP 12,Wildcard])) = 0
val test9c_6 = count_some_var ("x",(TupleP [Wildcard,Variable "str",Wildcard])) = 0
val test9c_7 = count_some_var ("x",(TupleP [Wildcard,Variable "x",Wildcard])) = 1
val test9c_8 = count_some_var ("x",(TupleP [Wildcard,Variable "x",Wildcard,Variable "x"])) = 2
val test9c_9 = count_some_var ("x",(ConstructorP("pattern",(TupleP [Wildcard,Variable "x",Wildcard])))) = 1
val test9c_10 = count_some_var ("x",(ConstructorP("x",(TupleP [Wildcard,Variable "x",Wildcard])))) = 1

val extract_1 = extract (Variable("x")) = ["x"]
val extract_2 = extract (Wildcard) = []
val extract_3 = extract (TupleP [Variable("abc"),Variable("ab")]) = ["abc","ab"]
val extract_4 = extract (ConstructorP("x",Variable "y")) = ["y"]

val distinct_1 = distinct ["x"] = true
val distinct_2 = distinct [] = true
val distinct_3 = distinct ["a","a"] = false
val distinct_4 = distinct ["a","b","a"] = false

val test10_1 = check_pat (Variable("x")) = true
val test10_2 = check_pat (Wildcard) = true
val test10_3 = check_pat (TupleP [Variable("abc"),Variable("ab")]) = true
val test10_4 = check_pat (TupleP [Variable("abc"),Variable("abc")]) = false
val test10_5 = check_pat (ConstructorP("x",Variable "x")) = true
val test10_6 = check_pat (TupleP [ConstructorP("x",Variable "y"),Variable("y")]) = false
val test10_7 = check_pat (TupleP [ConstructorP("x",Variable "y"),Variable("x")]) = true
val test10_8 = check_pat (TupleP [Wildcard,Variable "x",Wildcard]) = true
val test10_9 = check_pat (TupleP [Wildcard,Variable "x",Variable "y"]) = true
val test10_10 = check_pat (TupleP [Wildcard,Variable "x",Variable "x"]) = false
val test10_11 = check_pat (ConstructorP("x",(TupleP [Wildcard,Variable "x",Wildcard]))) = true
val test10_12 = check_pat (ConstructorP("x",(TupleP [Wildcard,Variable "x",ConstructorP("y",Variable "y")]))) = true
val test10_13 = check_pat (ConstructorP("x",(TupleP [Wildcard,Variable "x",ConstructorP("y",Variable "x")]))) = false
val test10_14 = check_pat (ConstructorP("x",(TupleP [Wildcard,Variable "x",ConstructorP("y",TupleP [Variable "y"])]))) = true
val test10_15 = check_pat (ConstructorP("x",(TupleP [Wildcard,Variable "x",ConstructorP("y",TupleP [Variable "z"])]))) = true
val test10_16 = check_pat (ConstructorP("x",(TupleP [Wildcard,Variable "x",ConstructorP("y",TupleP [Variable "x"])]))) = false
val test10_17 = check_pat (ConstructorP("x",(ConstructorP("y",TupleP [Variable "x",Variable "y"])))) = true
val test10_18 = check_pat (ConstructorP("x",(ConstructorP("y",TupleP [Variable "x",Variable "x"])))) = false
val test10_19 = check_pat (TupleP [Wildcard,Variable "x",TupleP [Variable "y"]]) = true

val test11_1 = match (Const(1),UnitP) = NONE
val test11_2 = match (Unit,Wildcard) = SOME []
val test11_3 = match (Const(4),Variable "a") = SOME [("a",Const(4))]
val test11_4 = match (Unit,Variable "a") = SOME [("a",Unit)]
val test11_5 = match (Unit,UnitP) = SOME []
val test11_6 = match (Const(4),ConstP(8)) = NONE
val test11_7 = match (Const(1),ConstP(1)) = SOME []
val test11_8 = match (Const(1),Variable "s") = SOME [("s",Const(1))]
val test11_9 = match (Const(1),TupleP [Wildcard]) = NONE
val test11_10 = match (Const(1),TupleP [ConstP 1]) = NONE
val test11_11 = match (Tuple [Unit],TupleP [UnitP]) = SOME []
val test11_12 = match (Tuple [Tuple [Unit]],TupleP [TupleP[UnitP]]) = SOME []
val test11_13 = match (Tuple [Tuple [Unit]],TupleP [TupleP[UnitP,Variable "x"]]) = NONE
val test11_14 = match (Tuple [Const(1),Tuple [Unit]],TupleP [ConstP 1,TupleP[UnitP]]) = SOME []
val test11_15 = match (Tuple [Const(1),Tuple [Unit,Const(2)]],TupleP [ConstP 1,TupleP[UnitP,Variable("s")]]) = SOME [("s",Const(2))]
val test11_16 = match (Tuple [Const(1),Tuple [Unit,Const(2)]],TupleP [ConstP 2,TupleP[UnitP,Variable("s")]]) = NONE
val test11_17 = match (Tuple [Const(1),Tuple [Unit,Const(2)]],TupleP [ConstP 1,TupleP[UnitP,Variable("s"),Wildcard]]) = NONE

val test12_0 = first_match Unit [UnitP] = SOME []
val test12_1 = first_match Unit [Variable ("s")] = SOME [("s",Unit)]
val test12_2 = first_match (Tuple [Const(1),Tuple [Unit,Const(2)]]) [(TupleP [ConstP 1,TupleP[UnitP,Variable("s")]])] = SOME [("s",Const(2))]
val test12_3 = first_match (Tuple [Const(1),Tuple [Unit,Const(2)]]) [(TupleP [ConstP 1,TupleP[UnitP,ConstP 3]])] = NONE
