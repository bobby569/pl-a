(* Coursera Programming Languages, Homework 3, Provided Code *)

exception NoAnswer

(* 1 *)
val only_capitals = List.filter (fn x => Char.isUpper(String.sub(x,0)))

(* 2 *)
val longest_string1 = foldl (fn (cur,acc) => if String.size cur > String.size acc then cur else acc) ""

(* 3 *)
val longest_string2 = foldl (fn (cur,acc) => if String.size cur >= String.size acc then cur else acc) ""

(* The helper *)
fun longest_string_helper f (xs : string list) = 
	foldl (fn(cur,acc) => if f(String.size cur, String.size acc) then cur else acc) "" xs

(* 4a *)
val longest_string3 = longest_string_helper (fn (a,b) => a > b)

(* 4b *)
val longest_string4 = longest_string_helper (fn (a,b) => a >= b)

(* 5 *)
val longest_capitalized = longest_string1 o only_capitals

(* 6 *)
val rev_string = String.implode o rev o String.explode

(* 7 *)
fun first_answer f xs =
	case xs of
		[] => raise NoAnswer
		| x::xs' =>
			case f x of
				NONE => first_answer f xs'
				| SOME v => v

(* 8 *)
fun all_answers f xs = 
	let
		fun helper(cur,acc,xs) = 
			case cur of
				NONE => NONE
				| SOME lst =>
					case xs of
						[] => SOME (acc @ lst)
						| x::xs' => helper(f x, acc @ lst, xs')
	in
		helper(SOME [], [], xs)
	end

(* pattern datatype *)
datatype pattern = Wildcard
		| Variable of string
		| UnitP
		| ConstP of int
		| TupleP of pattern list
		| ConstructorP of string * pattern

(* provided g function *)
fun g f1 f2 p =
	let 
		val r = g f1 f2 
	in
		case p of
			Wildcard          => f1 ()
			| Variable x        => f2 x
			| TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
			| ConstructorP(_,p) => r p
			| _                 => 0
	end

(* 9a *)
val count_wildcards = g (fn () => 1) (fn _ => 0)

(* 9b *)
val count_wild_and_variable_lengths = g (fn () => 1) String.size

(* 9c *)
fun count_some_var(s,p) = g (fn () => 0) (fn s' => if s' = s then 1 else 0) p

(* 10 *)
fun extract p =
	case p of
		Variable x => [x]
		| TupleP ps => foldl (fn (cur,acc) => acc @ extract cur) [] ps
		| ConstructorP(_,p) => extract p
		| _ => []

fun distinct xs = 
	case xs of
		[] => true
		| x::xs' => not (List.exists (fn s => s = x) xs') andalso distinct xs'

val check_pat = distinct o extract

(* valu datatype *)
datatype valu = Const of int
	    | Unit
	    | Tuple of valu list
	    | Constructor of string * valu

(* 11 *)
fun match(value,pat) = 
	case (value,pat) of
		(_,Wildcard) => SOME []
		| (_,Variable s) => SOME [(s,value)]
		| (Unit,UnitP) => SOME []
		| (Const x,ConstP y) => if x=y then SOME [] else NONE
		| (Tuple vs,TupleP ps) => 
			if length vs = length ps
			then all_answers match (ListPair.zip(vs,ps))
			else NONE
		| (Constructor(s2,v),ConstructorP(s1,p)) => if s1=s2 then match(v,p) else NONE
		| _ => NONE

(* 12 *)
fun first_match v ps = SOME (first_answer (fn x => match(v,x)) ps)
	handle _ => NONE
	
(**** for the challenge problem only ****)

datatype typ = Anything
	    | UnitT
	    | IntT
	    | TupleT of typ list
	    | Datatype of string

(**** you can put all your code here ****)
