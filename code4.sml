(* -- Matual Recursion -- *)

fun match xs = 
  let
    fun s_one xs = 
      case xs of
        [] => true
        | 1::xs' => s_two xs'
        | _ => false
    and s_two xs = 
      case xs of 
        [] => false
        | 2::xs' => s_one xs'
        | _ => false
  in
    s_one xs
  end

datatype t1 = Foo of int | Bar of t2
and t2 = Baz of string | Quux of t1

fun all_valid_t1 x =
  case x of
    Foo i => i <> 0
    | Bar y => all_valid_t2 y
and all_valid_t2 x = 
  case x of 
    Baz s => size s > 0
    | Quux y => all_valid_t1 y

(* Alternative way but slower *)
fun all_valid_t1_alter(f, x) =
  case x of
    Foo i => i <> 0
    | Bar y => f y

fun all_valid_t2_alter x =
  case x of
    Baz s => size s > 0
    | Quux y => all_valid_t1_alter(all_valid_t2_alter,y)

(* -- Modules for Namespace Management / class -- *)

structure MyMathLib = 
struct
  fun fact x = 
    if x = 0
    then 1
    else x * fact(x-1)

  fun half_pi = Math.pi / 2

  fun doubler x = 2 * x
end

val fac_10 = MyMathLib.fact 10

(* -- Signature / Interface -- *)

signature MATHLIB =
sig
  val fact : int -> int
  val half_pi : real
  (* val doubler : int -> int *) (* can hide bindings from clients *)
end

structure MyMathLib :> MATHLIB =
struct
  fun fact x = 
    if x = 0
    then 1
    else x * fact(x-1)

  fun half_pi = Math.pi / 2

  (* Private *)
  fun doubler x = 2 * x
end

(* -- Module Example -- *)

structure Rational =
struct
  datatype rational = Whole of int | Frac of int * int
  exception BadFrac

  fun gcd (x,y) = 
    if x = y
    then x
    else if x < y
    then gcd(x,y-x)
    else gcd(y,x)

  fun reduce r = 
    case r of 
      Whole _ => r
      | Frac (x,y) =>
        if x = 0
        then Whole 0
        else
          let
            val d = gcd(abs x,y)
          in
            if d = y
            then Whole(x div d)
            else Frac(x div d, y div d)
          end

  fun make_frac (x,y) = 
    if y = 0
    then raise BadFrac
    else if y < 0
    then reduce(Frac(~x,~y))
    then reduce(Frac(x,y))

  fun add(r1,r2) = 
    case (r1,r2) of
      (Whole(i),Whole(j)) => Whole(i+j)
      | (Whole(i),Frac(j,k)) => Frac(j+i*k,k)
      | (Frac(j,k),Whole(i)) => Frac(j+i*k,k)
      | (Frac(a,b),Frac(c,d)) => reduce(Frac(a*d+b*c,b*d))

  fun toString r = 
    case r of
      Whole i => Int.toString i
      | Frac(a,b) => (Int.toString a) ^ "/" ^ (Int.toString b)
end

(* -- Signature Example -- *)

signature RATIONAL_A = 
sig
  (* Potential initializer, which may cause trouble *)
  datatype rational = Frac of int * int | Whole of int 
  exception BadFrac
  val make_frac : int * int -> rational
  val add : rational * rational -> rational
  val toString : rational -> string
end

signature RATIONAL_B =
sig
  type rational (* type now abstract *)
  exception BadFrac
  val Whole : int -> rational 
  val make_frac : int * int -> rational
  val add : rational * rational -> rational
  val toString : rational -> string
end
