(* -- Functions as Arguments -- *)

fun n_times(f,x,n) = 
  if n = 0
  then x
  else f (n_times(f,x,n-1))

fun increment x = x + 1
fun double x = 2 * x

fun addition(x,n) = n_times(increment,x,n)
fun multipow2(x,n) = n_times(double,x,n)
fun nth_tail(x,n) = n_times(tl,x,n)

(* -- Anonymous Functions -- *)

fun triple x = 3 * x

fun triple_n_times1(x,n) = n_times(triple,x,n)
fun triple_n_times2(x,n) = let fun triple x = 3 * x in n_times(triple,x,n) end
fun triple_n_times3(x,n) = n_times((let fun triple x = 3 * x in triple end),x,n)
fun triple_n_times4(x,n) = n_times((fn x => 3 * x),x,n)

(* -- Map and Filter -- *)

fun map(f,xs) = 
  case xs of
    [] => []
    | x::xs' => (f x) :: map(f,xs')

fun filter(f,xs) =
  case xs of
    [] => []
    | x::xs' => 
      if f x 
      then x::(filter(f,xs')) 
      else filter(f,xs')

(* -- Closures & Recomputation -- *)

fun allShorterThan1 (xs,s) = filter (fn x => String.size x < (print "!"; String.size s), xs)

fun allShorterThan2 (xs,s) =
  let 
	  val i = (print "!"; String.size s)
  in
	  filter(fn x => String.size x < i, xs)
  end

(* -- Fold/Reduce & More Closures -- *)

fun fold(f,acc,xs) = 
  case xs of
    [] => acc
    | x::xs' => fold(f,f(acc,x),xs')

fun sum' xs = fold((fn (x,y) => x+y),0,xs)
fun all_pos xs = fold((fn (x,y) => x andalso y > 0),true,xs)
fun range_cnt(xs,lo,hi) = fold((fn (x,y) => x + if y >= lo andalso y <= hi then 1 else 0),0,xs)

(* -- Combining Function -- *)

fun compose(f,g) = fn x => f (g x)

fun sqrt i = Math.sqrt(Real.fromInt(abs i))
fun sqrt i = (Math.sqrt o Real.fromInt o abs) i
val sqrt = Math.sqrt o Real.fromInt o abs

infix !>
fun x !> f = f x
fun sqrt i = i !> abs !> Real.fromInt !> Math.sqrt

fun backup(f,g) = 
  fn x => 
    case f x of 
      NONE => g x
      | SOME y => y

fun backup(f,g) = 
  fn x => f x
    handle _ => g x

(* -- Currying -- *)

val sorted = fn x => fn y => fn z => z >= y andalso y >= x
val t1 = sorted 7 9 11

fun sorted_nicer x y z = z >= y andalso y >= x
val t2 = sorted_nicer 7 9 11

fun fold f acc xs = 
  case xs of
    [] => acc
    | x::xs' => fold f f(acc,x) xs'

fun range i j = if i > j then [] else i :: range (i+1) j
val countup = range 1

fun exists predicate xs = 
  case xs of  
    [] => false
    | x::xs' => predicate x orelse exists predicate xs'

val hasZero = exists (fn x => x = 0)

fun curry f = fn x => fn y => f(x,y)
fun curry f x y = f (x,y)
fun uncurry f (x,y) = f x y

(* -- Reference -- *)

val x = ref 42
val y = ref 42
val z = x
val _ = x := 43
val w = (!y) + (!z) (* 85 *)

(* -- Callback -- *)

val cbs : (int -> unit) list ref = ref []

fun onKeyEvent f = cbs := f::(!cbs)

fun onEvent e =
  let
    fun loop fs =
      case fs of
        [] => ()
        | f::fs' => (f e; loop fs')
  in
    loop (!cbs)
  end

val timesPressed = ref 0
val _ = onKeyEvent (fn _ => timesPressed := (!timesPressed) + 1)

fun printIfPress i =
  onKeyEvent (fn j => 
    if i = j
    then print (Int.toString i ^ "Pressed\n")
    else ())

val _ = printIfPressed 4
val _ = printIfPressed 11
val _ = printIfPressed 23

(* Doc *)

(* http://sml-family.org/Basis/manpages.html *)
(* https://www.cs.princeton.edu/~appel/smlnj/basis/ *)

(* -- Abstract Data Type -- *)

datatype set = S of {
  insert : int -> set,
  member : int -> bool,
  size : unit -> int
}

val empty_set = 
  let
    fun make_set xs =
      let
        fun contains i = List.exists(fn j => i = j) xs
      in
        S {
          insert = fn i => 
            if contains i
            then make_set xs
            else make_set(i::xs),
          member = contains,
          size = fn () => length xs
        }
      end
  in
    make_set []
  end

fun use_sets () =
  let
    val S s1 = empty_set
    val S s2 = (#insert s1) 34
    val S s3 = (#insert s2) 34
    val S s4 = (#insert s3) 19
  in
    if (#member s4) 42
    then 99
    else if (#member s4) 19
    then 17 + (#size s3) ()
    else 0
  end

(* -- Without Closure -- *)

datatype 'a mylist = 
  Empty
  | Cons of 'a * ('a mylst)

fun map f xs = 
  case xs of
    Empty => Empty
    | Cons(x,xs') => Cons(f x, map f xs')

fun filter f xs = 
  case xs of
    Empty => Empty
    | Cons(x,xs') =>
      if f x
      then Cons(x, filter f xs')
      else filter f xs'

fun length xs = 
  case xs of
    Empty => 0
    | Cons(_,xs') => 1 + length xs'
