(* 1 *)
fun alternate(lst : int list) =
	let
		fun helper(lst : int list, sign : int) = 
			if null lst
			then 0
			else hd lst * sign + helper(tl lst, ~sign)
	in
		helper(lst, 1)
	end

(* 2 *)
fun min_max(lst : int list) = 
	if null (tl lst)
	then (hd lst, hd lst)
	else
		let
			val tl_min_max = min_max(tl lst)
		in
			if hd lst < #1 tl_min_max andalso hd lst > #2 tl_min_max
			then (hd lst, hd lst)
			else if hd lst < #1 tl_min_max
			then (hd lst, #2 tl_min_max)
			else if hd lst > #2 tl_min_max
			then (#1 tl_min_max, hd lst)
			else tl_min_max
		end

(* 3 *)
fun cumsum(lst : int list) = 
	let
		fun helper(lst : int list, cur : int) = 
			if null lst
			then []
			else (cur + hd lst) :: helper(tl lst, cur + hd lst)
	in
		helper(lst, 0)
	end

(* 4 *)
fun greeting(name : string option) = 
	if isSome name
	then "Hello there, " ^ valOf name ^ "!"
	else "Hello there, you!"
	
(* 5 helper *)
fun repeat_helper(v : int, freq : int) = 
	if freq = 0
	then []
	else v :: repeat_helper(v, freq-1)
	
(* 5 *)
fun repeat(lst : int list, freq : int list) = 
	if null lst orelse null freq
	then []
	else repeat_helper(hd lst, hd freq) @ repeat(tl lst, tl freq)
	
(* 6 *)
fun addOpt(p1 : int option, p2 : int option) = 
	if isSome p1 andalso isSome p2
	then SOME (valOf p1 + valOf p2)
	else NONE
	
(* 7 *)
fun addAllOpt(lst : int option list) = 
	let
		fun helper(lst : int option list, acc : int option) = 
			if null lst
			then acc
			else if isSome(hd lst) andalso isSome(acc)
			then helper(tl lst, SOME(valOf(hd lst) + valOf(acc)))
			else if isSome(hd lst) andalso isSome(acc) = false
			then helper(tl lst, SOME(valOf(hd lst)))
			else helper(tl lst, acc)
	in
		helper(lst, NONE)
	end
	
(* 8 *)
fun any(lst : bool list) = 
	if null lst
	then false
	else hd lst orelse any(tl lst)
	
(* 9 *)
fun all(lst : bool list) = 
	if null lst
	then true
	else hd lst andalso all(tl lst)
	
(* 10 *)
fun zip(l1 : int list, l2 : int list) =
	if null l1 orelse null l2
	then []
	else (hd l1, hd l2) :: zip(tl l1, tl l2)
	
(* 11 *)
fun zipRecycle(l1 : int list, l2 : int list) = 
	let
		fun helper(lst1 : int list, lst2 : int list, e1 : bool, e2 : bool) = 
			let
				val lst1' = if null lst1 then l1 else lst1
				val lst2' = if null lst2 then l2 else lst2
				val e1 = e1 orelse null lst1
				val e2 = e2 orelse null lst2
			in
				if e1 andalso e2
				then []
				else (hd lst1',hd lst2')::helper(tl lst1',tl lst2',e1,e2)
			end
	in
		if null l1 orelse null l2
		then []
		else helper(l1,l2,null l1,null l2)
	end

(* 12 *)
fun zipOpt(l1 : int list, l2 : int list) =
	if length l1 = length l2
	then SOME zip(l1,l2)
	else NONE

(* 13 *)
fun lookup(pr : (string * int) list, s2 : string) = 
	if null pr
	then NONE
	else if #1 (hd pr) = s2
	then SOME #2 (hd pr)
	else lookup(tl pr, s2)

(* 14 *)
fun splitup(xs : int list) = 
	let
		fun helper(ori : int list, pos : int list, neg : int list) = 
			if null ori
			then (pos, neg)
			else if hd ori >= 0
			then helper(tl ori, pos @ [hd ori], neg)
			else helper(tl ori, pos, neg @ [hd ori])
	in
		helper(xs, [], [])
	end

(* 15 *)
fun splitAt(xs : int list, x : int) = 
	let
		fun helper(ori : int list, pos : int list, neg : int list) = 
			if null ori
			then (pos, neg)
			else if hd ori >= x
			then helper(tl ori, pos @ [hd ori], neg)
			else helper(tl ori, pos, neg @ [hd ori])
	in
		helper(xs, [], [])
	end

(* 16 *)
fun isSorted(xs : int list) = 
	if length xs < 2
	then true
	else hd xs < hd (tl xs) andalso isSorted(tl xs)

(* 17 *)
fun isAnySorted(xs : int list) = 
	let
		fun helper(xs : int list, trend : bool) = 
			if length xs < 2
			then true
			else (hd xs < hd (tl xs)) = trend andalso isAnySorted(tl xs, trend)
	in
		if length xs < 2
		then true
		else helper(tl xs, hd xs < hd (tl xs))
	end

(* 18 *)
fun sortedMerge(l1 : int list, l2 : int list) = 
	if null l1 andalso null l2
	then []
	else if null l1 orelse null l2
	then (if null l1 then l2 else l1)
	else if hd l1 < hd l2
	then (hd l1) :: sortedMerge(tl l1, l2)
	else (hd l2) :: sortedMerge(l1, tl l2)

(* 19 *)
fun qsort(xs : int list) = 


(* 20 *)
fun divide(xs : int list) = 
	let
		fun helper(ori : int list, l1 : int list, l2 : int list, idx : int) = 
			if null ori
			then (pos, neg)
			else if idx = 0
			then helper(tl ori, l1 @ [hd ori], l2, 1-idx)
			else helper(tl ori, l1, l2 @ [hd ori], 1-idx)
	in
		helper(xs, [], [], 0)
	end

(* 21 *)
fun not_so_quick_sort(xs : int list) = 

(* 22 *)
fun fullDivide(k : int, n : int) = 
	let
		fun helper(k : int, n : int, d : int) = 
			if n mod k = 0
			then fullDivide(k, n div k, d+1)
			else (d, n)
	in
		helper(k,n,0)
	end

(* 23 *)
fun factorize(x : int) =
	let
		fun helper(x : int, k : int) = 
			if x <= 1
			then []
			else
				let
					val tmp = fullDivide(k,x)
				in
					if #1 tmp = 0
					then helper(#2 tmp, k+1)
					else (k, #1 tmp)::helper(#2 tmp, k+1)
				end
	in
		helper(x,2)
	end

(* 24 helper *)
fun pow(n : int, k : int) = 
	if k = 0
	then 1
	if k = 1
	then n
	else
		let
			val tmp = pow(n, k div 2)
		in
			if k mod 2 = 1
			then tmp * tmp * n
			else tmp * tmp
		end

(* 24 *)
fun multiply(ts : (int * int) list) = 
	if null ts
	then 1
	else pow(hd ts) * tl ts

(* 25 *)


