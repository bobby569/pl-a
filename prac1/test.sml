fun countdown(x : int) = 
  if x = 0
  then []
  else x :: countdown(x-1)

fun range(x: int) = 
  let 
    fun count(from : int) = 
      if from = x-1
      then [x-1]
      else from :: count(from+1)
  in
    count 0
  end

val test1_1 = alternate([]) = 0
val test1_2 = alternate([1,2,3,4]) = ~2
val test1_3 = alternate([0,0,0]) = 0
val test1_4 = alternate([1,2,3]) = 2

val test2_1 = min_max([0]) = (0,0)
val test2_2 = min_max([~1,1]) = (~1,1)
val test2_3 = min_max([1,~1]) = (~1,1)
val test2_4 = min_max(range(10)) = (0,9)
val test2_5 = min_max(countdown(10)) = (1,10)
val test2_6 = min_max([5,6,4,7,3,8,2,9,1]) = (1,9)

val test3_1 = cumsum([]) = []
val test3_2 = cumsum([1]) = [1]
val test3_3 = cumsum([1,2,3,5]) = [1,3,6,11]
val test3_4 = cumsum([4,3,2,1]) = [4,7,9,10]

val test4_1 = greeting(NONE) = "Hello there, you!"
val test4_2 = greeting(SOME "Bobby") = "Hello there, Bobby!"

val test5_1 = repeat([1,2,3],[4,0,3]) = [1,1,1,1,3,3,3]
val test5_2 = repeat([1,2,3],[0,0,0]) = []
val test5_3 = repeat([1,2,3],[0]) = []
val test5_4 = repeat([1,2,3],[0,0,0,0]) = []
val test5_5 = repeat([1,2,3],[4,0,3,1]) = [1,1,1,1,3,3,3]

val test6_1 = addOpt(SOME 1, SOME 2) = SOME 3
val test6_2 = addOpt(SOME 1, NONE) = NONE
val test6_3 = addOpt(NONE, SOME 1) = NONE
val test6_4 = addOpt(NONE, NONE) = NONE

val test7_1 = addAllOpt([SOME 1, NONE, SOME 3]) = SOME 4
val test7_2 = addAllOpt([]) = NONE
val test7_3 = addAllOpt([NONE, NONE]) = NONE
val test7_4 = addAllOpt([SOME 1, NONE, SOME ~1]) = SOME 0

val test8_1 = any([]) = false
val test8_2 = any([false, false]) = false
val test8_3 = any([false, true]) = true
val test8_4 = any([true, true]) = true

val test9_1 = all([]) = true
val test9_2 = all([false, false]) = false
val test9_3 = all([false, true]) = false
val test9_4 = all([true, true]) = true

val test10_1 = zip([1,2,3],[4,6]) = [(1,4),(2,6)]
val test10_2 = zip([1,2,3],[]) = []
val test10_3 = zip([4],[1,2,3]) = [(4,1)]
val test10_4 = zip([],[1,2,3]) = []
val test10_5 = zip([],[]) = []

val test11_1 = zipRecycle([1,2],[3,4,5]) = [(1,3),(2,4),(1,5)]
val test11_2 = zipRecycle([1,2,3],[1,2,3,4,5,6,7]) = [(1,1),(2,2),(3,3),(1,4),(2,5),(3,6),(1,7)]
val test11_3 = zipRecycle([1,2,3],[]) = []
val test11_4 = zipRecycle([1,2,3],[1]) = [(1,1),(2,1),(3,1)]
val test11_5 = zipRecycle([],[]) = []
val test11_6 = zipRecycle([1,2],[2,3]) = [(1,2),(2,3)]