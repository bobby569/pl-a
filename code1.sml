(* -- Our first program -- *)

val x = ~34;
(* static env: x-->int *)
(* dynamic env: x-->-34 *)

val abs_x = if x < 0 then 0 - x else x;
val abs_x' = abs x;

(* -- Functions -- *)

fun pow(x:int, y:int) = 
  if y = 0
  then 1
  else x * pow(x, y-1)

fun cube(x: int) = pow(x, 3)

(* -- Data Structure - Pairs and Tuple -- *)

fun swap(pr : int*int) = (#2 pr, #1 pr)
fun divmod(x: int, y: int) = (x div y, x mod y)
fun sort_pair(pr: int*int) = 
  if (#1 pr) < (#2 pr)
  then pr
  else (#2 pr, #1 pr)

(* -- Data Structure - List -- *)

val x = [7,8,9];
val x1 = 5 :: x;      (* [5,7,8,9] *)

val empty = null x;   (* false, fn : 'a  list -> bool *)
val head = hd x;      (* 7, fn : 'a list -> 'a *)
val tail = tl x;      (* [8,9], fn : 'a list -> 'a list *)

(* -- List Functions -- *)

fun sum(xs : int list) = 
  if null xs
  then 0
  else hd(xs) + sum(tl xs)

fun countdown(x : int) = 
  if x = 0
  then []
  else x :: range(x-1)

fun append(xs : int list, ys : int list) = 
  if null xs
  then ys
  else hd xs :: append(tl xs, ys)

fun firsts(xs : (int * int) list) = 
  if null xs
  then []
  else (#1 (hd xs)) :: firsts(tl xs)

fun seconds(xs : (int * int) list) = 
  if null xs
  then []
  else (#2 (hd xs)) :: seconds(tl xs)

fun sum_pair (xs : (int * int) list) =
  sum (firsts xs) + sum (seconds xs)

(* -- Let Expression -- *)

fun silly() = 
  let
    val x = 1
  in 
    (let val x = 2 in x+1 end) + (let val y = x+2 in y+1 end)
  end

(* -- Nested Function -- *)

fun countup(x : int) = 
  let
    fun count(from : int, to : int) =
      if from = to
      then [to]
      else from :: count(from+1, to)
  in
    count(1, x)
  end

fun countup2(x : int) = 
  let
    fun count(from : int) =
      if from = x
      then [x]
      else from :: count(from+1)
  in 
    count 1
  end

fun range(x: int) = 
  let 
    fun count(from : int) = 
      if from = x-1
      then [x-1]
      else from :: count(from+1)
  in
    count 0
  end

(* -- Let Efficiency -- *)

fun max(xs : int list) = 
  if null xs
  then 0
  else if null (tl xs)
  then hd xs
  else
  let
    val tl_max = max(tl xs)
  in 
    if hd xs > tl_max
    then hd xs
    else tl_max
  end

(* -- Options -- *)

fun max1(xs : int list) = 
  if null xs
  then NONE
  else
    let 
      val tl_max = max1(tl xs)
    in
      if isSome tl_max andalso valOf tl_max > hd xs
      then tl_max
      else SOME (hd xs)
    end

fun max2(xs : int list) = 
  if null xs
  then NONE
  else
    let 
      fun max_nonempty(xs : int list) = 
        if null (tl xs)
        then hd xs
        else
          let 
            val tl_max = max_nonempty(tl xs)
          in
            if hd xs > tl_max
            then hd xs
            else tl_max
          end
    in
      SOME (max_nonempty xs)
    end
